import os
from face_analysis import EmotionDetection
import  cv2

image_list = os.listdir('./data/')
print(len(image_list))
buffer_size = 1
input_shapes = (48, 48)
model_name = 'model_v6_23.hdf5'
labels = {'Angry': 0, 'Sad': 5, 'Neutral': 4, 'Disgust': 1, 'Surprise': 6, 'Fear': 2, 'Happy': 3}
# image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
emotion_predictor = EmotionDetection(labels, model_name, input_shapes)
image_buffer = []
for image in image_list:
    image_buffer.append(cv2.imread('./data/'+image))
total_buffers = len(image_list)/buffer_size
final_buffer_array = []
index = 0
start = 0
end = buffer_size
while (index < total_buffers):
    buffer_array = image_buffer[start:end]
    final_buffer_array.append(buffer_array)
    start += 3
    end += 3
    index += 1
    print(len(buffer_array))
    print(buffer_array[0].shape)
    result = emotion_predictor.prediction(buffer_array)
    print(result)


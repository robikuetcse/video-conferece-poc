'''
The MIT License (MIT)
Copyright (c) 2013 Dave P.
'''

import signal
import sys
import ssl
from SimpleWebSocketServer import WebSocket, SimpleWebSocketServer, SimpleSSLWebSocketServer
from optparse import OptionParser
import base64
import numpy as np
import cv2
from PIL import Image
import io
from matplotlib import cm
import matplotlib.pyplot as plt
import re
import time
import json
from face_analysis import EmotionDetection


class SimpleEcho(WebSocket):

   def handleMessage(self):
      print (self.data)
      self.sendMessage(self.data)

   def handleConnected(self):
      print (self)
      pass

   def handleClose(self):
      print (self)
      pass

clients = []
input_shapes = (48, 48)
model_name = 'model_v6_23.hdf5'
labels = {'Angry': 0, 'Sad': 5, 'Neutral': 4, 'Disgust': 1, 'Surprise': 6, 'Fear': 2, 'Happy': 3}
emotion_detector = EmotionDetection(labels, model_name, input_shapes)

i = 0

class SimpleChat(WebSocket):
   def handleMessage(self):
      global i
      print('received image from ')
      #print(self.address[0])

      prediction = {}
      if len(self.data)>100:
          image_data = re.sub('^data:image/.+;base64,', '', self.data)
          # npimg = np.fromstring(image_data, dtype=np.uint8)
          image = Image.open(io.BytesIO(base64.b64decode(image_data)))
          npimg = np.asarray(image)
          prediction = emotion_detector.prediction([npimg])
          print(prediction)
          # if  i < 1000:
          #   image.save('data/' + str(time.time()) + '.jpg')
          #   i += 1


      for client in clients:
         # if client != self:
         try:
            client.sendMessage(json.dumps(dict(client_id=self.address[0]+':'+str(self.address[1]), msg=str(prediction))))
         except Exception as e:
             print('Exception: ', e)



   def handleConnected(self):
      print (self.address, 'connected')
      for client in clients:
         # client.sendMessage(self.address[0]+':'+ self.address[1] + u' - connected')
         client.sendMessage(json.dumps(dict(client_id=self.address[0]+':'+str(self.address[1]), msg="connected")))
      clients.append(self)

   def handleClose(self):
      clients.remove(self)
      print (self.address, 'closed')
      for client in clients:
         # client.sendMessage(self.address[0]+':'+ self.address[1] + u' - disconnected')
         client.sendMessage(json.dumps(dict(client_id=self.address[0]+':'+str(self.address[1]), msg="disconnected")))


if __name__ == "__main__":

   parser = OptionParser(usage="usage: %prog [options]", version="%prog 1.0")
   parser.add_option("--host", default='192.168.82.16', type='string', action="store", dest="host", help="hostname (localhost)")
   parser.add_option("--port", default=8000, type='int', action="store", dest="port", help="port (8000)")
   parser.add_option("--example", default='echo', type='string', action="store", dest="example", help="echo, chat")
   parser.add_option("--ssl", default=0, type='int', action="store", dest="ssl", help="ssl (1: on, 0: off (default))")
   parser.add_option("--cert", default='./cert.pem', type='string', action="store", dest="cert", help="cert (./cert.pem)")
   parser.add_option("--key", default='./key.pem', type='string', action="store", dest="key", help="key (./key.pem)")
   parser.add_option("--ver", default=ssl.PROTOCOL_TLSv1, type=int, action="store", dest="ver", help="ssl version")

   (options, args) = parser.parse_args()

   print(options)
   print(args)
   cls = SimpleEcho
   if options.example == 'chat':
      cls = SimpleChat

   server = SimpleWebSocketServer(options.host, options.port, cls, .05)

   def close_sig_handler(signal, frame):
      server.close()
      sys.exit()

   signal.signal(signal.SIGINT, close_sig_handler)

   server.serveforever()

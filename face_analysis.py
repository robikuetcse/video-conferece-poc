import cv2
import numpy as np
# from datetime import datetime
import json
from keras.models import load_model
# import os
from FSANET_model import *

class EmotionDetection:

    def __init__(self,model_path, labels_dict, input_shape ):
        self.labels = labels_dict
        self.input_shape = (48,48)
        self.model = load_model(model_path)
    def detect(self, images):
        faces = np.empty((len(images), self.input_shape[0], self.input_shape[1],1))
        results = []
        for i, face in enumerate(images):
            # x1, y1, x2, y2, w, h = d.left(), d.top(), d.right() + 1, d.bottom() + 1, d.width(), d.height()

            cropped=cv2.resize(face, (self.input_shape[0], self.input_shape[1]))
            faces[i, :, :,:] = np.reshape(cropped,[self.input_shape[0],self.input_shape[1],1])

            face = np.expand_dims(faces[i, :, :,:], axis=0)
            predicted_class = np.argmax(self.model.predict(face))
            label_map = dict((v, k) for k, v in self.labels.items())
            predicted_label = label_map[predicted_class]
            results.append(predicted_label)

            face = face.squeeze()

        return results


class HeadPoseEstimation:

    def __init__(self):
        self.img_size = 64
        self.stage_num = [3, 3, 3]
        self.lambda_local = 1
        self.lambda_d = 1
        self.img_idx = 0
        self.detected = ''  # make this not local variable
        self.time_detection = 0
        self.time_network = 0
        self.time_plot = 0
        self.skip_frame = 5  # every 5 frame do 1 detection and network forward propagation
        self.ad = 0.6

        # Parameters
        num_capsule = 3
        dim_capsule = 16
        routings = 2
        stage_num = [3, 3, 3]
        lambda_d = 1
        num_classes = 3
        image_size = 64
        num_primcaps = 7 * 3
        m_dim = 5
        S_set = [num_capsule, dim_capsule, routings, num_primcaps, m_dim]

        model1 = FSA_net_Capsule(image_size, num_classes, stage_num, lambda_d, S_set)()
        model2 = FSA_net_Var_Capsule(image_size, num_classes, stage_num, lambda_d, S_set)()

        num_primcaps = 8 * 8 * 3
        S_set = [num_capsule, dim_capsule, routings, num_primcaps, m_dim]

        model3 = FSA_net_noS_Capsule(image_size, num_classes, stage_num, lambda_d, S_set)()

        print('Loading models ...')

        weight_file1 = './pre-trained/300W_LP_models/fsanet_capsule_3_16_2_21_5/fsanet_capsule_3_16_2_21_5.h5'
        model1.load_weights(weight_file1)
        print('Finished loading model 1.')

        weight_file2 = './pre-trained/300W_LP_models/fsanet_var_capsule_3_16_2_21_5/fsanet_var_capsule_3_16_2_21_5.h5'
        model2.load_weights(weight_file2)
        print('Finished loading model 2.')

        weight_file3 = './pre-trained/300W_LP_models/fsanet_noS_capsule_3_16_2_192_5/fsanet_noS_capsule_3_16_2_192_5.h5'
        model3.load_weights(weight_file3)
        print('Finished loading model 3.')

        inputs = Input(shape=(64, 64, 3))
        x1 = model1(inputs)  # 1x1
        x2 = model2(inputs)  # var
        x3 = model3(inputs)  # w/o
        avg_model = Average()([x1, x2, x3])
        self.model = Model(inputs=inputs, outputs=avg_model)


    def predict(self, images):
        #print('Number of images:',len(images))
        faces = np.empty((len(images), self.img_size, self.img_size, 3))
        results=[]
        for i, face in enumerate(images):
            # x1, y1, x2, y2, w, h = d.left(), d.top(), d.right() + 1, d.bottom() + 1, d.width(), d.height()

            faces[i,:,:,:]=cv2.resize(face, (self.img_size, self.img_size))
            faces[i, :, :, :] = cv2.normalize(faces[i, :, :, :], None, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX)
            face = np.expand_dims(faces[i, :, :, :], axis=0)
            p_result = self.model.predict(face)
            results.append(p_result)
            face = face.squeeze()

        return results
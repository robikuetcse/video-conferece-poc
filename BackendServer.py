'''
The MIT License (MIT)
Copyright (c) 2013 Dave P.
'''
import os
import psutil

import signal
import sys
import ssl
from SimpleWebSocketServer import WebSocket, SimpleWebSocketServer, SimpleSSLWebSocketServer
from optparse import OptionParser
import base64
import numpy as np
import cv2
from PIL import Image
import io
import re
import time
import cv2
import json
import io
from utils import json_default
from face_analysis import EmotionDetection
from face_analysis import HeadPoseEstimation

input_shapes = (48, 48)
model_path = 'model/emotion_best_weights.h5'
labels = {'Angry': 0, 'Sad': 5, 'Neutral': 4, 'Disgust': 1, 'Surprise': 6, 'Fear': 2, 'Happy': 3}
#emotion_detector = EmotionDetection(labels, model_name, input_shapes)
head_pose_model=HeadPoseEstimation()
emotion_detector=EmotionDetection(model_path, labels, input_shapes)
face_cascade = cv2.CascadeClassifier('lbpcascade_frontalface_improved.xml')

result={}
sources={}




class SimpleEcho(WebSocket):

   def handleMessage(self):
      print (self.data)
      self.sendMessage(self.data)

   def handleConnected(self):
      print (self)
      pass

   def handleClose(self):
      print (self)
      pass

clients = []

prediction='Nothing'
i = 0

class SimpleChat(WebSocket):
   def handleMessage(self):
      global i
      print('received image from:', self.address)
      #print(self.address[0])

      prediction = {}
      if len(self.data) > 100:
          image_data = re.sub('^data:image/.+;base64,', '', self.data)
          # npimg = np.fromstring(image_data, dtype=np.uint8)
          image = Image.open(io.BytesIO(base64.b64decode(image_data)))
          npimg = np.asarray(image)
          open_cv_image = npimg[:, :, ::-1].copy()
          gray_img = cv2.cvtColor(open_cv_image, cv2.COLOR_BGR2GRAY)
          detected = face_cascade.detectMultiScale(gray_img, 1.1)

          if len(detected) > 0:
              #print(detected)
              x, y, w, h = detected[0]
              face_cropped_p = open_cv_image[y:y + h, x:x + w]
              face_cropped_e = gray_img[y:y + h, x:x + w]
              sources[self.address[0] + '_' + str(self.address[1])] = [face_cropped_p, face_cropped_e, detected[0]]
              #print(sources)
              # pose = list(head_pose_model.predict([face_cropped_p])[0])
              # print
      if len(sources.keys())>0:
        self.process_request()
      for client in clients:
         # if client != self:
         try:
             client.sendMessage(json.dumps(dict(client_id=self.address[0] + ':' + str(self.address[1]),
                                                msg=json.dumps(result, default=json_default))))
            #client.sendMessage(json.dumps(dict(client_id=self.address[0]+':'+str(self.address[1]), msg=str(prediction))))
         except Exception as e:
             print('Exception: ', e)

   def process_request(self):
       efaces = []
       pfaces = []
       bboxs = []
       for face_cropped_p, face_cropped_e, bbox in sources.values():
           efaces.append(face_cropped_e)
           pfaces.append(face_cropped_p)
           bboxs.append(bbox)

       emotions = emotion_detector.detect(efaces)
       poses = head_pose_model.predict(pfaces)
       poses = [list(pose) for pose in poses]

       print('users:', sources.keys())

       boxes = []

       for box in bboxs:
           box = list(box)
           boxes.append([int(val) for val in box])

       result['users'] = list(sources.keys())
       result['detections'] = boxes
       result['emotions'] = emotions
       result['poses'] = poses = [list(pose) for pose in poses]

   def handleConnected(self):
      print (self.address, 'connected')
      for client in clients:
         # client.sendMessage(self.address[0]+':'+ self.address[1] + u' - connected')
         client.sendMessage(json.dumps(dict(client_id=self.address[0]+':'+str(self.address[1]), msg="connected")))
      clients.append(self)

   def handleClose(self):
      clients.remove(self)
      print (self.address, 'closed')
      for client in clients:
         # client.sendMessage(self.address[0]+':'+ self.address[1] + u' - disconnected')
         client.sendMessage(json.dumps(dict(client_id=self.address[0]+':'+str(self.address[1]), msg="disconnected")))


if __name__ == "__main__":

   parser = OptionParser(usage="usage: %prog [options]", version="%prog 1.0")
   parser.add_option("--host", default='localhost', type='string', action="store", dest="host", help="hostname (localhost)")
   parser.add_option("--port", default=8000, type='int', action="store", dest="port", help="port (8000)")
   parser.add_option("--example", default='chat', type='string', action="store", dest="example", help="echo, chat")
   parser.add_option("--ssl", default=0, type='int', action="store", dest="ssl", help="ssl (1: on, 0: off (default))")
   parser.add_option("--cert", default='./cert.pem', type='string', action="store", dest="cert", help="cert (./cert.pem)")
   parser.add_option("--key", default='./key.pem', type='string', action="store", dest="key", help="key (./key.pem)")
   parser.add_option("--ver", default=ssl.PROTOCOL_TLSv1, type=int, action="store", dest="ver", help="ssl version")

   (options, args) = parser.parse_args()

   print(options)
   print(args)
   cls = SimpleEcho
   if options.example == 'chat':
      cls = SimpleChat

   server = SimpleWebSocketServer(options.host, options.port, cls, .05)
   process = psutil.Process(os.getpid())
   print(process.memory_info().rss)
   def close_sig_handler(signal, frame):
      server.close()
      sys.exit()

   signal.signal(signal.SIGINT, close_sig_handler)

   server.serveforever()


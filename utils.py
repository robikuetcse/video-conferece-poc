import numpy as np


def get_initial_weights(output_size):
    b = np.zeros((2, 3), dtype='float32')
    b[0, 0] = 1
    b[1, 1] = 1
    W = np.zeros((output_size, 6), dtype='float32')
    weights = [W, b.flatten()]
    return weights

def json_default(obj):
   if type(obj).__module__ == np.__name__:
      if isinstance(obj, np.ndarray):
         return obj.tolist()
      else:
         return obj.item()
   raise TypeError('Unknown type:', type(obj))
